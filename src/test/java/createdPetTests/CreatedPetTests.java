package createdPetTests;

import data.PetEndpoints;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;
import pet.PetResponse;
import petResponse.Category;
import testBase.TestBase;

import java.util.stream.Stream;

import static data.PetEndpoints.*;
import static helper.RequestHelper.createPetDto;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;

public class CreatedPetTests extends TestBase {
    
    
    Category category = new Category();
    
    
    @Test
    public void shouldCreatedPetWithNameTest() {
        PetResponse petResponse = petApiClient.createdPet(createPetDto(PET_NAME, 1,PET_STATUS))
                                          .then()
                                          .statusCode(200)
                                          .assertThat()
                                          .extract().response().as(PetResponse.class);
        
        assertThat("ID was not generated nad it's NULL", petResponse.getId(), is(notNullValue()));
        assertThat("Ids are not equal", petResponse.getName(), equalTo(PET_NAME));
        petResponse.toString();
        System.out.println(petResponse);
        
        
        
    }
    
    
    @Test
    public void shouldCreatedPetTest() {
        
        category.setName("Dinosaurs");
        
        given()
                .contentType(ContentType.JSON)
                .when()
                .body(createPetDto(PET_NAME, 1,PET_STATUS))
                .post(URL + PET)
                .then()
                .statusCode(200)
                .and()
                .assertThat()
                .body(containsString(PET_NAME))
                .extract().response().prettyPrint();
    }
    
    
    
}
