package findPetAndStoreTests;

import org.testng.annotations.Test;
import responseTests.ResponseTests;
import testBase.TestBase;

import static data.PetEndpoints.*;
import static io.restassured.RestAssured.given;

public class GetByStoreTests extends TestBase {
    
    @Test
    public void shouldFindByStore() {
        given()
                .when()
                .get(URL+STORE_INVENTORY)
                .then()
                .statusCode(200).extract().response().prettyPrint();
    }
    
    
  
    
    @Test
    public void shouldGetStoreByOrderId() {
        given()
                .when()
                .get(URL + STORE_ORDER+ORDER_ID)
                .then()
                .statusCode(200).extract().response().prettyPrint();
    }
}
