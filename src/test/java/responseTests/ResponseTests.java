package responseTests;

import helper.PetController;
import org.testng.annotations.Test;
import pet.PetResponse;
import petResponse.RequestPetModel;
import testBase.TestBase;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static data.PetEndpoints.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ResponseTests extends TestBase {
    
    RequestPetModel petModel = new RequestPetModel();
    PetController petController = new PetController(petModel);
    
    @Test
    public void shouldCreatedPetAndCheckResponseTest() {
        
        petModel = petController.addNewPet(petName, petId, PET_STATUS);
        PetResponse[] petResponses = petController.findPetResponse(FIND_BY_STATUS + STATUS_DEAD);
        
        List<PetResponse> filteredResponse = Arrays.asList(petResponses);
        
        List<PetResponse> newList = filteredResponse.stream()
                                            .filter(name -> name.getName() == null || name.getName().equalsIgnoreCase(petName))
                                            .collect(Collectors.toList());
        System.out.println(newList.size());
        
    }
    
    @Test
    public void shouldUpdatePetTest() {
        
        petController.updatePet(newPetName, petId, PET_STATUS);
        PetResponse responses = petController.findPetById(String.valueOf(petId));
        List<PetResponse> filter = Arrays.asList(responses);
        List<PetResponse> newFilter = filter.stream()
                                              .filter(name -> name.getName() == null || name.getName().equalsIgnoreCase(newPetName))
                                              .collect(Collectors.toList());
        assertThat(responses.getName(), equalTo(newPetName));
        System.out.println(newFilter);
        
    }
}
