package testBase;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import rest.PetApiClient;

import java.util.Properties;

public class TestBase {
    PropertiesConfiguration properties;
    
    {
        try {
            properties = new PropertiesConfiguration("pet-store.properties");
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }
    
    protected PetApiClient petApiClient;
    
    
    public String petName;
    public long petId = 12132;
    
    @BeforeClass
    public void setUp() {
        petApiClient = new PetApiClient();
        
        petName = properties.getString("petName");
    }
    
    @AfterClass
    public void tearDown() {
    
    }
    
    @DataProvider
    public Object[][] dataProvider(){
        return new Object[][]{
                {3213,"Cat"},
                {2121,"Dog"}};
    }
    
    @DataProvider
    public Object[][] invalidData(){
        return new Object[][]{
                {3213,"Cat"},
                {2121,"Dog"}};
    }
}
