package helper;

import data.PetEndpoints;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import pet.PetResponse;
import petResponse.RequestPetModel;

import java.util.List;
import java.util.Map;

import static data.PetEndpoints.*;
import static helper.RequestHelper.createPetDto;
import static helper.RequestHelper.createPetDtoDataProvider;
import static helper.RequestHelper.updatePetDto;
import static io.restassured.RestAssured.given;

public  class PetController {
    private RequestSpecification requestSpecification;
    public RequestPetModel pet;
    private String name;
    private long petId;
    
    
   public PetController(RequestPetModel pet) {
        
        requestSpecification = new RequestSpecBuilder()
                                       .setBaseUri(ADD_PET_URL)
                                       .setContentType(ContentType.JSON)
                                       .log(LogDetail.ALL).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder()
                                        .expectContentType(ContentType.JSON)
                                        .expectStatusCode(200)
                                        .build();
        RestAssured.defaultParser = Parser.JSON;
        this.pet = pet;
        
    }
    public PetResponse addNewPetWithDataProvider(long id,String name){
       return given(requestSpecification)
               .body(createPetDtoDataProvider(id,name))
               .post(ADD_PET_URL)
               .as(PetResponse.class);
    }
    
    
    public RequestPetModel addNewPet(String name, long petId,String petStatus) {
        return given(requestSpecification)
                       .body(createPetDto(name, petId, petStatus))
                       .post(ADD_PET_URL)
                       .as(RequestPetModel.class);
    }
    
    public PetResponse[] findPetResponse(String sourcePass){
       return  given(requestSpecification)
                       .get(ADD_PET_URL + sourcePass)
                       .as(PetResponse[].class);
    }
    
    public PetResponse findPetById(String petId){
       return given(requestSpecification)
               .get(FIND_PET_BY_ID+petId)
               .as(PetResponse.class);
    }
    public RequestPetModel updatePet(String name,long petId,String petStatus){
       return given(requestSpecification)
                      .body(updatePetDto(name, petId, petStatus))
                      .put(UPDATE_PET_URL)
                      .as(RequestPetModel.class);
    }
    
    
    public Response deletePet(long petId) {
        return RestAssured.given()
                       .delete(URL + PET + "/" + petId)
                       .then()
                       .extract().response();
    }
    public Response searchPetIsDeleted(long petId) {
        return RestAssured.given()
                       .when()
                       .delete(URL + PET + "/" + petId)
                       .then()
                       .statusCode(404)
                       .assertThat()
                       .extract().response();
    }
    
}
